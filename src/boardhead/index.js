import React from 'react';
import Timer from '../timer';

class Boardhead extends React.Component {

  constructor(props) {
    super(props);
    this.state =  { };
  }

render() {
    let minutes = Math.floor(this.props.counter / 60);
    let seconds = this.props.counter - minutes * 60 || 0;
    let formattedSeconds = seconds < 10 ? "0" + seconds : seconds;
    let time = minutes +":" + formattedSeconds;

    return (
      <div className="boardhead">
        {/* <div className="flag-count">{this.props.flagCount}</div> */}
        <button className="reset btn btn-info btn-lg">Reset</button>
        {/* <div className="timer">{time}</div> */}
        <Timer />
      </div>
    )
  }

}



export default Boardhead;
