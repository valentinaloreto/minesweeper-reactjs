import React from 'react';


class Timer extends React.Component {

  constructor(props) {
    super(props);
    this.state = {
      time : 0
    };
  }

  componentDidMount() {
    this.timerID = setInterval(
      () => this.tick(),
      1000
    );
  }

  tick() {
    this.setState({ time: this.state.time + 1 })
  }

  componentWillUnmount() {
    clearInterval(this.timerID)
  }

  render() {
    return (
      <div>Seconds: <br/>{ this.state.time }</div>
    );
  }

}



export default Timer;
