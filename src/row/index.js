import React from 'react';
import Cell from '../cell';
function Row(props) {
  let arrayOfCells = props.cells.map((data, index)=>{ return  <Cell key={index} cell={data} open={ props.open } changeStatus={ props.changeStatus } status={props.status}/> });
  return <div className="single-row">{arrayOfCells}</div>;
}
export default Row;
