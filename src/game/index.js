import React from 'react';
import Board from '../board';
import Boardhead from '../boardhead';

class Game extends React.Component {

  constructor(props) {
    super(props);
    this.state =  {
      status: 'running',
      rows: 10,
      cols: 10,
      mines: 10,
      //flags: 10,
      //counter: 0,
      openCells: 0
    };
  }

  openCellsIncrement(number) {
    var sum = this.state.openCells + number
    this.setState({ openCells: sum });
  }

  changeStatus(newstatus) {
    this.setState({ status: newstatus });
  }

  render() {
    return (
      <div className="game">
        <h1 className="title">MineSweeper</h1>
        <p className="open-cells-count">Open Cells: {this.state.openCells}</p>
        <Board rows={this.state.rows} cols={this.state.cols} mines={this.state.mines} openCells={this.state.openCells}
          incrementOpenCellsCount={ this.openCellsIncrement.bind(this) } changeStatus={ this.changeStatus.bind(this)} status={this.state.status}/>
      </div>
    )
  }

}



export default Game;


