import React from 'react';

class Cell extends React.Component {

  constructor(props) { super(props); }

  handleOpenedCell(cell){
    if (cell.hasMine) {
      return <div className="cell open">@</div>;
    } else if( cell.mineCount === 0) {
      return <div className="cell open hideme" >.</div>;
    } else {
      return <div className="cell open">{cell.mineCount}</div>;
    }
  };

  renderCell() {
    let cell = this.props.cell;


    if (cell.isOpen) {
      return this.handleOpenedCell(cell);
    } else if(this.props.status === 'done') {
      return <div className="cell open hideme">.</div>;
    } else {
      return <div className="cell hideme" onClick={ () => {this.props.open(cell)} }>.</div>;
    }
  };

  render() {
    let cellToRender = this.renderCell();
    return cellToRender;
  };
}

export default Cell;
