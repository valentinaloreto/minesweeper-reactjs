import React from 'react';
import Row from '../row';

class Board extends React.Component {

  constructor(props) {
    super(props);
    this.state =  { board: this.createBoard() };
  }

  //**********************BEFORE**********************************//
  createBoard() {
    let board = [];
    for (let i =0; i < this.props.rows; i++) {
      board[i] = [];
      for (let j =0; j < this.props.cols ; j++) {
        board[i][j] = {
          x: j,
          y: i,
          mineCount: 0,
          isOpen: false,
          hasMine: false,
          hasFlag: false
        }
      }
    }
    this.addMinesToBoard(board);
    return board;
  };

  addMinesToBoard(board) {
    for (let i = 0; i < this.props.mines; i++) {
      let randomRow = Math.floor( Math.random() * this.props.rows );
      let randomCol = Math.floor( Math.random() * this.props.cols );
      let cell = board[randomRow][randomCol];
      cell.hasMine ? i-- : cell.hasMine = true;
    }
  };

  restartGame() {
    this.setState({ board: this.createBoard() });
    this.props.incrementOpenCellsCount(-(this.props.openCells) );
    this.props.changeStatus('running');
  }

  //**********************DURING**********************************//
  open(cell) {

    let asyncCountMines = new Promise(resolve => {
      let minesAroundACell = this.findMinesAround(cell)
      resolve(minesAroundACell);
    })
    asyncCountMines.then( numberMinesAroundCell => {
      if (cell.hasMine) {
        this.openMinedCell(cell);
      } else if(cell.isOpen === false) {
        this.openMinelessCell(cell, numberMinesAroundCell, this.state.board);
      }
    });

  };

  openMinedCell(cell) {
    cell.isOpen = true;
    let board = this.state.board;

    //console.log(this.props.changeStatus);
    //console.log(this.props.status);
    this.props.changeStatus('done');
    //console.log(this.props.status);

    this.setState({ board });
    alert('YOU LOST');
  }

  openMinelessCell(cell, numberMinesAroundCell, board) {
    let openedCellss = 0;
    cell.isOpen = true;
    openedCellss++;

    cell.mineCount = numberMinesAroundCell;
    this.setState({ board });
    if (!cell.hasMine && numberMinesAroundCell === 0) { this.checkCellsAround(cell); }
    this.props.incrementOpenCellsCount(openedCellss);
  }

  findMinesAround(cell) {
    let minesInProximity = 0;
    for(let row = -1; row <= 1; row++) {
      for(let col= -1; col <= 1; col++) {
        if (cell.y + row >= 0 && cell.x + col >= 0) {
          if (cell.y + row < this.state.board.length && cell.x + col < this.state.board[0].length ) {
            if ( this.state.board[cell.y + row][cell.x + col].hasMine && !( row === 0 && col === 0) ) {
              minesInProximity++;
            }
          }
        }
      }
    }

    return minesInProximity;
  }

  checkCellsAround(cell) {
    let board = this.state.board;
    for(let row = -1; row <= 1; row++) {
      for(let col= -1; col <= 1; col++) {
        if (cell.y + row >= 0 && cell.x + col >= 0) {
          if (cell.y + row <  board.length && cell.x + col < board[0].length ) {
            if(!board[cell.y + row][cell.x + col].hasMine && !board[cell.y + row][cell.x + col].isOpen ) {
              this.open(board[cell.y + row][cell.x + col]);
            }
          }
        }
      }
    }
  }

  //**********************RENDER**********************************//
  render() {
    let rows = this.state.board.map((row,index)=> { return <Row cells={row} key={index} open={ this.open.bind(this) } changeStatus={ this.props.changeStatus } status={this.props.status}/> })
    return (
      <div className="wrapper">
        <button className="reset btn btn-info btn-lg" onClick={ () => { this.restartGame()} } >Reset</button>
        <div className="board">{rows}</div>
      </div>
    )
  }

}

export default Board;
