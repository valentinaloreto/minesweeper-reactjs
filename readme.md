Minesweeper - Buscaminas


Descripción del juego

Es un videojuego para un jugador inventado por Robert Donner en 1989. El objetivo del juego es
despejar un campo de minas sin detonar ninguna. El campo de minas puede variar de tamaño por
lo general y el jugador tiene la posibilidad de descubrir una casilla o de marcarla porque sospecha
que se encuentra una mina en la misma.


Reglas

● El juego consiste en despejar todas las casillas de una pantalla que no oculten una mina al hacer
clic sobre algún espacio del tablero.
● Al momento de hacer clic la casilla revelará si posee una mina. En caso de no tener revelará un
número, el cual indica la cantidad de minas que hay en las casillas circundantes. Así, si una casilla
tiene el número 3, significa que de las ocho casillas que hay alrededor (si no es en una esquina o
borde) hay 3 con minas y 5 sin minas.
● Si ninguna de las casillas vecinas tiene mina, la casilla se revela sin número y ésta, a su vez,
descubre automáticamente al resto de las casillas "sin número" (es decir, sin minas circundantes)
alrededor de ellas.
● Si se descubre una casilla con una mina se pierde la partida.
● Se puede poner una marca en las casillas que el jugador piensa que hay minas para ayudar a
descubrir las que están cerca y/o no olvidar que ya se revisó la casilla.

¿Cómo ganar?

Cuando el jugador revela toda las casillas vacías de minas, el juego se termina y el jugador gana.

¿Cómo perder?

Si el jugador descubre una mina el jugador pierde.

¿Qué se espera del juego?

● El juego debe tener una pantalla de bienvenidad y presentación al juego.
● El juego se debe desarrollar en un tablero 10x10 y debe contener un número de 20 minas.
● El juego debe poder jugarse según todas las reglas descritas arriba.
● El juego debe jugarse con el "mouse", el botón izquierdo para descubrir la casilla y el botón
derecho para "marcar" la casilla.
● El tablero debe tener un look&feel similar al juego Mines de Ubuntu.
● Una casilla marcada no puede ser descubierta directamente, debe ser desmarcada primero para
poder ser descubierta.
● Se debe anunciar cuando el jugador gane o pierda el juego y el tablero debe bloquerse de
cualquier acción al ocurrir cualquiera de esos eventos.
● El juego debe permitir reiniciarse sin necesidad de tener que recargar la página o servidor.
Opcionales/Adicionales
● Que el jugador pueda elegir el tamaño del tablero.
● Que el jugador pueda elegir la cantidad de minas.
● Que al iniciar el juego el jugador nunca se tope con una mina para reducir el porcentaje de
"suerte" en el juego.

Recomendaciones

● Se recomienda abordar el problema mediante descomposición, es decir, separando el problema
en subproblemas más simples cuyas soluciones puedan ser implementadas con independencia.
● Diseñe la arquitectura y los algoritmos de su programa antes de codificar. Recuerde que el papel
siempre es buena idea.
● En caso de tener dudas con el enunciado, no dude en consultarlas con los mentores.
